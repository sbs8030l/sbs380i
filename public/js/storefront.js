var loadScript = function (url, callback) {
  /* JavaScript that will load the jQuery library on Google's CDN.
     We recommend this code: http://snipplr.com/view/18756/loadscript/.
     Once the jQuery library is loaded, the function passed as argument,
     callback, will be executed. */
};

var myAppJavaScript = function ($) {
  /* Your app's JavaScript here.
     $ in this scope references the jQuery object we'll use.
     Don't use 'jQuery', or 'jQuery191', here. Use the dollar sign
     that was passed as argument.*/
  $("body").append(
    "<p>Your app is using jQuery version " + $.fn.jquery + "</p>"
  );

  var check = $(".event_identifier").val();
  var sell_type = $("#sell_type").val();

  if (sell_type === "cross_sell") {
    jQuery.post("/cart/clear.js", function () {
      alert("cross sell");
    });
  }

  if (check !== "") {
    if (Shopify.theme.name == "Pop") {
      $("#addToCart-product-template").after(
        '<a id="custom_cart" class="btn btn--large btn--full">Add to Cart</a>'
      );
      $("#addToCart-product-template").remove();
    } else if (Shopify.theme.name == "Venture") {
      $("#AddToCart-product-template").after(
        '<a id="custom_cart" class="btn btn--full">Add to Cart</a>'
      );
      $("#AddToCart-product-template").remove();
    } else if (Shopify.theme.name == "Minimal") {
      $("#AddToCart").after(
        '<a id="custom_cart" class="btn btn--wide">Add to Cart</button>'
      );
      $("#AddToCart").remove();
    } else if (Shopify.theme.name == "Brooklyn") {
      $("#AddToCart--product-template").after(
        '<a id="custom_cart" class="btn btn--add-to-cart">Add to Cart</a>'
      );
      $("#AddToCart--product-template").remove();
    } else if (Shopify.theme.name == "Narrative") {
      $(".product__add-to-cart-button").after(
        '<a id="custom_cart" class="btn btn--to-secondary btn--full">Add to Cart</a>'
      );
      $(".product__add-to-cart-button").remove();
    } else if (Shopify.theme.name == "Supply") {
      $("#addToCart-product-template").after(
        '<a id="custom_cart" class="btn">Add to Cart</a>'
      );
      $("#addToCart-product-template").remove();
    } else if (Shopify.theme.name == "Jumpstart") {
      $("#AddToCart-product-template").after(
        '<a id="custom_cart" class="btn btn--fill btn--regular btn--color">Add to Cart</a>'
      );
      $("#AddToCart-product-template").remove();
    } else if (Shopify.theme.name == "Boundless") {
      $("#AddToCart-product-template").after(
        '<a id="custom_cart" class="btn">Add to Cart</a>'
      );
      $("#AddToCart-product-template").remove();
    } else if (Shopify.theme.name == "Debut" || Shopify.theme.name == "debut") {
      $("#AddToCart-product-template").after(
        '<a id="custom_cart" class="btn">Add to Cart</a>'
      );
      $("#AddToCart-product-template").remove();
    } else if (
      Shopify.theme.name == "Simple" ||
      Shopify.theme.name == "simple"
    ) {}
  } else {
    $("button[name='add']").attr("id", "custom_cart");
  }

  if (timer2 !== "") {
    $("#demo1").insertBefore("#custom_cart");
  }
  var time_color = $("#timer_color").val();
  var replace_item_btn_color = $("#replace_item_btn_color").val();
  var fake_stock = $("#fake_stock").val();
  var replace_item_text = $("#replace_item_text").val();

  $("#demo").css("color", time_color);
  $(".add-to-cart").css({
    "background-color": replace_item_btn_color,
    color: "#ffffff"
  });
  $(".fake_stock").text(fake_stock);
  $(".add-to-cart:contains(Take This Offer)").text(replace_item_text);

  var timer2 = $("#timer2").val();
  var timer_color2 = $("#timer_color2").val();
  var fake_stock2 = $("#fake_stock2").val();

  if (timer2 !== "") {
    $("#custom_cart").before(
      "<p>Available Stock: <span class='fake_stock2'>" +
      fake_stock2 +
      "</span></p>"
    );
    $("#demo1").css({
      color: timer_color2,
      "font-size": "26px"
    });
  }

  $(".ecom-upsell__triger-product-container").remove();
  $(".ecom-product__price--deleted").remove();
  // $(".ecom-modal__btn-close").remove();
  $(".ecom-upsell__intro").remove();
  $(document).on("click", "#custom_cart", function () {
    var shop_url = Shopify.shop;
    var product_val = $("#event_identifier").val();
    product_val2 = product_val.substr(14);
    product_val3 = product_val2.replace(/\s+/g, "-");
    if (product_val3 == "") {
      var variant_id = $("#product_id").val();
      add_to_cart_func(variant_id, true);
    }
    $.getJSON("/products/" + product_val3 + ".js", function (product) {
      $("#event_identifier").after(
        '<input type="hidden" value="' +
        product.variants["0"]["id"] +
        '" id="add-upsell-to-cart" class="add-upsell-to-cart">'
      );
      $("#ecom-modal").removeClass("ecom-modal--animated");
      $("#ecom-modal").show();
      $(".ecom-product__title").text(product.title);
      console.log(product.price.length);

      if (
        product.compare_at_price_max !== "" ||
        product.compare_at_price_max !== 0
      ) {
        $(".current_price").text("$" + product.compare_at_price_max / 100);
        $(".current_price").text("$" + product.price / 100);
      } else {
        $(".current_price").text("$" + product.price / 100);
      }

      $(".ecom-product__image").attr("src", product.featured_image);
    });
  });

  $(document).on("click", ".add-to-cart", function () {
    var variant_id = $("#add-upsell-to-cart").val();
    $.ajax({
      method: "POST",
      dataType: "json",
      data: {
        quantity: 1,
        id: variant_id
      },
      url: "/cart/add.js",
      success: function (data) {
        var variant_id2 = $("#product_id").val();
        if (sell_type !== "cross_sell") {
          add_to_cart_func(variant_id2, true);
        } else {
          window.location.href = "/cart";
        }
      }
    });
  });

  $(".ecom-upsell__button:contains(No Thanks)").click(function () {
    $("#ecom-modal").hide();
    var variant_id = $("#product_id").val();
    add_to_cart_func(variant_id, true);
  });

  $(".ecom-upsell__button:contains(Continue)").click(function () {
    $("#ecom-modal").hide();
    var variant_id = $("#product_id").val();
    add_to_cart_func(variant_id, true);
  });

  $("#ecom-modal__btn-close:contains(x)").click(function () {
    $("#ecom-modal").hide();
    var variant_id = $("#product_id").val();
    add_to_cart_func(variant_id, true);
  });

  function add_to_cart_func(variant, redirect) {
    var domain = Shopify.shop;

    $.ajax({
      method: "POST",
      dataType: "json",
      data: {
        quantity: 1,
        id: variant
      },
      url: "/cart/add.js",
      success: function (data) {
        if (redirect) {
          if (data["status"] != 404) {
            window.location.href = "/cart";
          }
        }
      }
    });
  }

  function clear_and_cross(variant, redirect) {
    var domain = Shopify.shop;
    clear_shop();
    $.ajax({
      method: "POST",
      dataType: "json",
      data: {
        quantity: 1,
        id: variant
      },
      url: "/cart/add.js",
      success: function (data) {
        if (redirect) {
          if (data["status"] != 404) {
            window.location.href = "/cart";
          }
        }
      }
    });
  }

  function clear_shop() {
    $.ajax({
      method: "POST",
      dataType: "json",
      url: "/cart/clear.js",
      success: function (data) {
        if (redirect) {
          return true;
        }
      }
    });
  }

  var dating = document.getElementById("timeanddate").value;
  if (dating != "") {
    var countDownDate = new Date(dating).getTime();
    console.log("1");
  } else {
    var dating2 = document.getElementById("timer2").value;
    var countDownDate = new Date(dating2).getTime();
    console.log("2");
  }
  // Set the date we're counting down to

  // Update the count down every 1 second
  var x = setInterval(function () {
    // Get todays date and time
    var now = new Date().getTime();

    // Find the distance between now an the count down date
    var distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor(
      (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
    );
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Output the result in an element with id='demo'
    document.getElementById("demo").innerHTML =
      days + "d " + hours + "h " + minutes + "m " + seconds + "s ";
    document.getElementById("demo1").innerHTML =
      days + "d " + hours + "h " + minutes + "m " + seconds + "s ";

    // If the count down is over, write some text
    if (distance < 0) {
      clearInterval(x);
      document.getElementById("demo").innerHTML = "EXPIRED";
      document.getElementById("demo1").innerHTML = "EXPIRED";
    }
  }, 1000);
  if (timer2 !== "") {
    $("#demo1").insertBefore("#custom_cart:contains(Add to Cart)");
  }
  if (timer2 == "") {
    $("#demo1").css("display", "none");
  }

  var popup_header_val = $("#popup_header_text").val();
  if (popup_header_val !== "") {
    $("#pop_header_text_para").text(popup_header_val);
  }
  // $('#demo1').insertBefore('.btn--wide:contains(Add to Cart)');
};

if (typeof jQuery === "undefined" || parseFloat(jQuery.fn.jquery) < 1.7) {
  loadScript(
    "//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js",
    function () {
      jQuery191 = jQuery.noConflict(true);
      myAppJavaScript(jQuery191);
    }
  );
} else {
  myAppJavaScript(jQuery);
}
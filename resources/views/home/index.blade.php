@extends('layouts.app')

@section('content')
	<section>
		<aside>
		<h2>Navigation</h2>
		<p>Click on these buttons to navigate</p>
		</aside>
		<article>
			<div class="card">
				<div class="column twelve">
					<h5>Click On<strong> Button to Navigate</strong></h5>
					<span class="button-group">
						<a href="#upsell" class="button">Upsell/Cross-sell Trigger</a>
						<!-- <a href="#countdown_form" class="button">Countdown Setting</a> -->
						<a href="#upsell_listing" class="button">Offer Listing</a>
						<!-- <a href="#countdown_listing" class="button">Countdown Listing</a> -->
						<!-- <a href="#change_theme" class="button">Change Theme</a> -->
					</span>
				</div>
			</div>
		</article>
	</section>

	<!-- <section id="change_theme">
	<aside>
	<h2>Action On Theme Change</h2>
	<p>Click Reload when changing the theme to apply functionality for new one.</p>
	</aside>
	<article>
		<div class="card">
			<div class="column two-thirds">
				<h5>Click On<strong> Theme Change</strong></h5>
				<span class="button-group">
					<a href="{{ url('/') }}/reload?op=reload" class="button secondary">Reload</a>
				</span>
			</div>
			<div class="column one-third" style="text-align:center;">
				
			</div>
		</div>
		<div class="column tewelve">
			<div class="alert notification">
				<dl>
					<dt>Action On Theme Change</dt>
					<dd>Please click Reload button to update your current Theme.</dd>
				</dl>
			</div>
		</div>
	</article>
</section> -->

	<section id="upsell">
		<aside>
			<h2>Upsell/Cross-Sell Trigger</h2>
			<p>Step 1: Select 1 product from the “Product” dropdown box.</p>
			<p>Step 2: Select between 1 to 3 products from the “Attached Upsell/Cross-sell product” dropdown box that you wish to Upsell or Cross-sell.</p>
			<p>Step 3: Choose your Sell Type.</p>
			<p>Step 4  Select your discount code that you created in your Shopify discount section.</p>
			<p>Step 5: Select your desired color button.</p>
			<p>Step 6: Type in your desired text in the “Offer Button Text” and “Pop Up Header Text”.</p>
			<p>Step 7: Hit the submit button and congratulations, you have successfully created your Upsell/Cross-sell.</p>
		</aside>
		<article>
			<div class="card">
			@if($success == "1")
			<div class="alert success">
				<dl>
					<dt>Offer Activated Successfully</dt>
					<dd>Offer is Activated Successfully. Please go to the product and check it out</dd>
				</dl>
			</div>
			@endif
				<h5>Upsell/Cross-Sell Trigger</h5>
				<form action="{{ url('/') }}/apply_offer" method="POST" id="upsell_form">
					@csrf
					<label for="">Product</label>
					<select data-placeholder="Choose a Country..." class="chosen-select" tabindex="2" name="trigger_product" id="" required>
						@foreach ($shop_products as $product)
							<option value="{{ $product->id }}-{{ $product->handle }}">{{ $product->title }}</option>
						@endforeach
					</select>
					</br>
					</br>
						<label for="">Sell Type</label>
						<select data-placeholder="Choose a Sell Type..." class="chosen-select" tabindex="2" name="sell_type" id="" required>
								<option value="cross_sell">Cross Sell</option>
								<option value="up_sell">Up Sell</option>
						</select>
					</br>
					</br>
						<label for="">Attached Upsell/Cross-sell product</label>
						<select data-placeholder="Choose a Country..." class="chosen-select" tabindex="2" name="offer_product" id="" required>
							@foreach ($shop_products as $product)
								<option value="{{ $product->id }}-{{ $product->title }}">{{ $product->title }}</option>
							@endforeach
						</select>
					</br>
					</br>
					<!-- </br>
					<label>Timer Day, Date and Time</label>
					<input type="text" name="timer" placeholder="May 28, 2018 22:10:25" required/>
					</br>
					<label>Timer Color</label>
					<input type="text" name="timer_color" placeholder="Enter Color Code: #000000" required/>
					</br>
					<label>Stock</label>
					<input type="text" name="fake_stock" placeholder="Enter stock in numbers" required/>
					</br> -->
					<label>Offer Button Color</label>
					<input type="text" name="replace_item_btn_color" placeholder="Enter Color Code: #000000" required/>
					</br>
					<label>Offer Button Text</label>
					<input type="text" name="replace_item_text" placeholder="Enter Text of the button" required/>
					</br>
					<label>Pop Up Header Text</label>
					<input type="text" name="popup_header_text" placeholder="Enter Pop Up Header Text" required/>
					</br>
					<input type="submit" value="Submit">
				</form>
			</div>
		</article>
  	</section>
	  <section id="upsell_listing">
		<aside>
  			<h2>Listing</h2>
			<p>This is the list of the active offering on this store made by this app</p>
		</aside>
		<article>
			<div class="card">
				@if($success == "4")
				<div class="alert success">
					<dl>
						<dt>Deleted Successfully</dt>
						<dd>Product Offer is deleted Successfully</dd>
					</dl>
				</div>
				@endif
				<h5>List of the Active Offering</h5>
				<table>
					<thead>
						<tr>
						<th>Meta Field ID</th>
						<th>Trigger Product</th>
						<th>Offer Product</th>
						<th>Sell Type</th>
						<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($upsell_product as $product)
							<tr>
								<td>{{ $product->metafieldId }}</td>
								<td><a href="https://{{session('myshopifyDomain')}}/products/<?php $trigger_name = substr($product->triggerProduct, 13);
																																																																				echo (str_replace(' ', '-', $trigger_name)); ?>">{!! substr($product->triggerProduct, 13) !!}</a></td>
								<td><a href="https://{{session('myshopifyDomain')}}/products/<?php $offer_name = substr($product->offerProduct, 13);
																																																																				echo (str_replace(' ', '-', $offer_name)); ?>">{!! substr($product->offerProduct, 13) !!}</a></td>
								<td>{{ $product->sellType }}</td>
								<td><a href="https://sbs8030l.xyz/delete_offer?meta_id={{ $product->metafieldId }}&trigger={!! substr($product->triggerProduct, 0, 12) !!}" class="button secondary icon-trash"></a></td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</article>
	</section>
	  <section id="countdown_form" style="display:none;">
		<aside>
			<h2>For Countdown Specific Products</h2>
			<p>If you want to show countdown of specific product then enter day formt</p>
		</aside>
		<article>
			<div class="card">
				@if($success == "2")
				<div class="alert success">
					<dl>
						<dt>Countdown Timer is Activated</dt>
						<dd>Countdown Timer is activated for this product. Check it out</dd>
					</dl>
				</div>
				@endif
				<h5>Countdown Specific Products</h5>
				<form action="{{ url('/') }}/countdown" method="POST">
					@csrf
					<label for="">Active Countdown on Product</label>
					<select data-placeholder="Active Countdown on Product..." class="chosen-select" tabindex="2" name="count_active_product" id="chosens-select" required>
						@foreach ($shop_products as $product)
							<option value="{{ $product->id }}">{{ $product->title }}</option>
						@endforeach
					</select>
					<input type="hidden" name="count_active_product_name" id="product_name" value="">
					</br>
					</br>
					<label>Countdown Timer</label>
					<input type="text" name="timer2" placeholder="May 28, 2018 22:10:25" required/>
				
					</br>
					<label>Countdown Timer Color</label>
					<input type="text" name="timer_color2" placeholder="Enter color code #000000" required/>
				
					</br>
					<label>Stock</label>
					<input type="text" name="fake_stock2" placeholder="Enter Stock" required/>

					</br>
					<input type="submit" value="Add">
				</form>
			</div>
		</article>
  	</section>
	<section id="countdown_listing"  style="display:none;">
		<aside>
  			<h2>Countdown Timer</h2>
			<p>This is the list of the product on which countdown timer is activated</p>
		</aside>
		<article>
			<div class="card">
				@if($success == "3")
				<div class="alert success">
					<dl>
						<dt>Deleted Successfully</dt>
						<dd>Countdown Timer is deleted from the product</dd>
					</dl>
				</div>
				@endif
				<h5>List of the Active Countdown Timer</h5>
				<table>
					<thead>
						<tr>
						<th>Product Name</th>
						<th>Timer</th>
						<th>Timer Color</th>
						<th>Stock</th>
						<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($countdown_data as $products)
							<tr>
								<td><a href="https://{{session('myshopifyDomain')}}/products/<?php $trigger_name = substr($products->productName, 13);
																																																																				echo (str_replace(' ', '-', $products->productName)); ?>">{{ $products->productName }}</a></td>
								<td>{{ $products->timer }}</td>
								<td>{{ $products->timerColor }}</td>
								<td>{{ $products->stock }}</td>
								<td><a href="https://sbs8030l.xyz/delete_timer?stock={{ $products->StockMetaField }}&timer={{ $products->timerMetaField }}&timerColor={{ $products->timerColorMetaField }}&productId={{ $products->productId }}" class="button secondary icon-trash" id="" ></button></td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</article>
	</section>

@endsection
